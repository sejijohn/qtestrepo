#Author: seji.john@anthem.com
@TEST8
@QTEST
Feature: As a shopper I should be able to launch Shopper portal URL

Scenario: User is able to launch shopper portal URL
Given a user lands into "https://shop.anthem.com/sales/eox/shop/demographics/home/snq?execution=e1s1" website
Then a user is able to verify "Basics" title