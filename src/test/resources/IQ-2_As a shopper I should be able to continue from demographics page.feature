#Author: seji.john@anthem.com
@TEST6
@QTEST
Feature: As a shopper I should be able to continue from demographics page

Scenario: User is able continue from demographic page
Given a user lands into "https://shop.anthem.com/sales/eox/shop/demographics/home/snq?execution=e1s1" website
When a user is able to verify "Basics" title 
Then a user is able to enter below details and click continue
| Zip Code       | 30002           |
| First Name     | John     |
| DOB            | 01/01/1980 |

