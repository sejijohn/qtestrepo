#Author: seji.john@anthem.com
@IQ-3
@QTEST
Feature: As a shopper I should be able to select a Medical plan

Scenario: User is able continue from demographic page
Given a user lands into "https://shop.anthem.com/sales/eox/shop/demographics/home/snq?execution=e1s1" website
And a user is able to verify "Basics" title 
And a user is able to enter below details and click continue
| Zip Code       | 30002           |
| First Name     | John     |
| DOB            | 01/01/1980 |
And a user is in "Estimate Cost Savings" page
When a user clicks Skip this step link


