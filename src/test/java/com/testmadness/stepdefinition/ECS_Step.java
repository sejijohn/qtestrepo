package com.testmadness.stepdefinition;

import com.testmadness.base.WebDriverBase;
import com.testmadness.pages.ESCPage;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.When;

public class ECS_Step extends WebDriverBase {
	ESCPage escPage;
	
	public ECS_Step(){
	     escPage = new ESCPage();
	}
	
	@Given("^a user is in \"([^\"]*)\" page$")
	public void a_user_is_in_page(String title) throws Throwable {
	    // Write code here that turns the phrase above into concrete actions
		escPage.verifyTitle(title);  
	}

	@When("^a user clicks Skip this step link$")
	public void a_user_clicks_Skip_this_step_link() throws Throwable {
	    // Write code here that turns the phrase above into concrete actions
		escPage.clickSkipThisStep();
	}

}
