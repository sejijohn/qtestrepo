package com.testmadness.stepdefinition;

import java.util.Map;

import com.testmadness.base.WebDriverBase;
import com.testmadness.pages.HomePage;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;

public class Home_Step extends WebDriverBase{
	HomePage homePage;
	
	public Home_Step() {
		homePage = new HomePage();
	}
	
	@Given("^a user lands into \"([^\"]*)\" website$")
	public void a_user_lands_into_website(String url) throws Throwable {
	    homePage.navigateToURL(url);
	    //WebDriverBase.getDriverInstance().close();
		//WebDriverBase.getDriverInstance().quit();
	}
	
	@When("^a user is able to verify \"([^\"]*)\" title$")
	public void a_user_is_able_to_verify_title(String title) throws Throwable {
	    // Write code here that turns the phrase above into concrete actions
		homePage.verifyTitle(title);
	}

	@Then("^a user is able to enter below details and click continue$")
	public void a_user_is_able_to_enter_below_details_and_click_continue(Map<String, String> basicDetails) throws Throwable {
	    // Write code here that turns the phrase above into concrete actions
		homePage.continueBasics(basicDetails);
	}

}
