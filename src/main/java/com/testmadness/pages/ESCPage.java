package com.testmadness.pages;

import org.junit.Assert;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

import com.testmadness.base.BasePage;

public class ESCPage extends BasePage {
	
	@FindBy(id = "hypertext/shop/estimatesavings/skipthisstep")
	private WebElement skipThisStep;
	public WebElement getSkipThisStep(){
		try{
			elementDetails.put(skipThisStep, "get skip This Step link");
		}catch(Exception e){
			
		}
		return skipThisStep;
	}
	
	public void verifyTitle(String title){
		String getTitle = getTitle().substring(0, 21);
	    Assert.assertTrue(title.trim().equals(getTitle));
		
	}
	
	public void clickSkipThisStep(){
		clickElement(getSkipThisStep());
	}

}
