package com.testmadness.pages;

import java.util.Map;

import org.junit.Assert;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import com.testmadness.base.BasePage;



public class HomePage extends BasePage {
	
	@FindBy(id = "demographics.zip5")
	private WebElement zipCode;
	public WebElement getZipCode(){
		try{
			elementDetails.put(zipCode, "get zipCode text box");
		}catch(Exception e){
			
		}
		return zipCode;
	}
	
	
	@FindBy(id = "demographics.applicants0.firstName")
	private WebElement firstName;
	public WebElement getFirstName(){
		try{
			elementDetails.put(firstName, "get first Name text box");
		}catch(Exception e){
			
		}
		return firstName;
	}
	
	@FindBy(id = "demographics.applicants0.dob")
	private WebElement dob;
	public WebElement getDob(){
		try{
			elementDetails.put(dob, "get first Name text box");
		}catch(Exception e){
			
		}
		return dob;
	}
	
	@FindBy(id = "demographics.applicants0.tobaccoUser1")
	private WebElement tobaccoUserNo;
	public WebElement getTobaccoUserNo(){
		try{
			elementDetails.put(tobaccoUserNo, "get tobacco User No radio");
		}catch(Exception e){
			
		}
		return tobaccoUserNo;
	}
	
	
	@FindBy(id = "button/shop/getaquote/next")
	private WebElement btnContinueBasics;
	public WebElement getBtnContinueBasics(){
		try{
			elementDetails.put(btnContinueBasics, "get Continue Basics button");
		}catch(Exception e){
			
		}
		return btnContinueBasics;
	}
	
	
	public void verifyTitle(String title){
		String getTitle = getTitle();
	    Assert.assertTrue(title.equals(getTitle));
		
	}
	
	public void continueBasics(Map<String, String> basicDetails) {
		EnterText(getZipCode(),basicDetails.get("Zip Code"));
		EnterText(getFirstName(),basicDetails.get("First Name"));
		EnterText(getDob(),basicDetails.get("DOB"));
		clickElement(getTobaccoUserNo());
		clickElement(getBtnContinueBasics());
		}

}
